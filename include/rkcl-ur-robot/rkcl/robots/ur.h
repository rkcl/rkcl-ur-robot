#pragma once

#include <rkcl/core.h>
#include <rkcl/drivers/ur_driver.h>
#include <rkcl/processors/collision_avoidance_sch.h>
#include <rkcl/processors/forward_kinematics_rbdyn.h>
#include <rkcl/processors/osqp_solver.h>
#include <rkcl/utils.h>
